-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: plinio
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `question`
--

DROP TABLE IF EXISTS `question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idsurvey` int(11) DEFAULT NULL,
  `number` int(11) NOT NULL,
  `text` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idsurvey_idx` (`idsurvey`),
  CONSTRAINT `idsurvey` FOREIGN KEY (`idsurvey`) REFERENCES `survey` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=168 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question`
--

LOCK TABLES `question` WRITE;
/*!40000 ALTER TABLE `question` DISABLE KEYS */;
INSERT INTO `question` VALUES (1,1,1,'How did you hear about the MAV ? '),(2,1,2,'Which is the reason for the visiting the museum?'),(3,1,3,'Approximately how long have you spent in the museum? '),(4,1,4,'What do you find particularly attractive about the museum? '),(5,1,5,'The price is reasonable '),(6,1,6,'MAV staff members were helpful and friendly'),(7,1,7,'Did you visit  archaeological site of Ercolano?   '),(9,1,8,'Rate your experience in museum\'s section: Bourbon Tunnel                     '),(10,1,8,'Rate your experience in museum\'s section: Public Buildings                    '),(11,1,8,'Rate your experience in museum\'s section: Domus'),(12,1,8,'Rate your experience in museum\'s section: Villa of the Papyri '),(14,1,9,'Rate  technology of museum: 3-D recreation'),(15,1,9,'Rate  technology of museum: Multi-projection     '),(16,1,9,'Rate  technology of museum: Sensory effects           '),(17,1,9,'Rate  technology of museum: Virtual guides       '),(18,1,9,'Rate  technology of museum: 5D Room                 '),(19,1,10,'Would you recommend to visit the museum to friends and relatives? '),(21,1,11,'Rate your  experience in MAV '),(22,1,9,'Rate  technology of museum: The movie The eruption of the Vesuvius'),(50,7,1,'Comment avez-vous entendu parler de MAV?'),(51,7,2,'Quelle est la raison de la visite du musée?'),(52,7,3,'Approximativement combien de temps avez-vous dépensé dans le musée?'),(53,7,4,'Qu\'est-ce qui vous plaît particulièrement dans le musée?'),(54,7,5,'Le prix est raisonnable'),(55,7,6,'Les membres du personnel de MAV étaient serviables et amicaux'),(56,7,7,'Avez-vous visité le site archéologique de Herculanum?'),(57,7,8,'Évaluez votre expérience en musée: Bourbon Tunnel'),(58,7,9,'Évaluer la technologie du musée: 3D'),(62,7,10,'Recommanderiez-vous de visiter le musée à des amis et des parents?'),(63,7,11,'Aimeriez-vous être informé de l\'activité du musée?'),(64,7,12,'Évaluez votre expérience à MAV'),(65,7,8,'Évaluez votre expérience en musée: Bâtiments publics'),(66,7,8,'Évaluez votre expérience en musée: Domus '),(67,7,8,'Évaluez votre expérience en musée: Villa des Papyrus'),(68,7,9,'Évaluer la technologie du musée: Multi-Projection     '),(69,7,9,'Évaluer la technologie du musée: Effets sensoriels   '),(70,7,9,'Évaluer la technologie du musée: Guides virtuelse  '),(71,7,9,'Évaluer la technologie du musée: Salle 5D'),(72,7,9,'Évaluer la technologie du musée: Le film “The eruption of the Vesuvius”'),(73,9,1,'¿Cómo usted oyó hablar el MAV?'),(74,9,2,'¿Cuál es el motivo de la visita al Museo?'),(75,9,3,'¿Aproximadamente cuánto tiempo pasaste en el museo?'),(76,9,4,'¿Qué te parece particularmente atractivo en el Museo?'),(77,9,5,'El precio es razonable'),(78,9,6,'Los miembros del personal de MAV eran serviciales y amable'),(79,9,7,'¿Visitó el sitio arqueológico de Ercolano? '),(80,9,8,'Valora tu experiencia en Museo: Túnel borbonico'),(82,9,8,'Valora tu experiencia en Museo: Edificios públicos '),(83,9,8,'Valora tu experiencia en Museo: Domus'),(84,9,8,'Valora tu experiencia en Museo: Villa de los Papiros  '),(85,9,9,'Evaluar la tecnología del museo: 3D'),(86,9,9,'Evaluar la tecnología del museo: Multi-Proyección  '),(87,9,9,'Evaluar la tecnología del museo: Efectos sensoriales   '),(88,9,9,'Evaluar la tecnología del museo: Guías virtuales  '),(89,9,9,'Evaluar la tecnología del museo: 5D Sala'),(90,9,9,'Evaluar la tecnología del museo: La película “The eruption of the Vesuvius”  '),(91,9,10,'¿Recomendaría visitar a amigos y parientes?'),(93,9,12,'Valora tu experiencia en MAV'),(94,8,1,'Wie haben Sie von der MAV gehört?'),(95,8,2,'Welches ist der Grund für den Besuch des Museums?'),(96,8,3,'Ungefähr wie lange haben Sie im Museum verbracht?'),(97,8,4,'Was, wenn überhaupt, finden Sie besonders attraktiv oder ansprechend über das Museum?'),(98,8,5,'Der Preis ist angemessen'),(99,8,6,'MAV Mitarbeiter waren hilfsbereit und freundlich'),(100,8,7,'Haben Sie die archäologische Stätte von Ercolano besucht?'),(101,8,8,'Bewerten Sie Ihre Erfahrung in der Museumsabteilung: Bourbon Tunnel     '),(102,8,8,'Bewerten Sie Ihre Erfahrung in der Museumsabteilung: Öffentliche Gebäude     '),(103,8,8,'Bewerten Sie Ihre Erfahrung in der Museumsabteilung: Domus  '),(104,8,8,'Bewerten Sie Ihre Erfahrung in der Museumsabteilung: Villa dei Papiri     '),(105,8,9,'Bewerten Sie die Technologie des Museums: 3-D Erholung '),(106,8,9,'Bewerten Sie die Technologie des Museums: Multi-Projektion  '),(107,8,9,'Bewerten Sie die Technologie des Museums: Sensorische Effekte  '),(108,8,9,'Bewerten Sie die Technologie des Museums: Virtuelle Leitfäden  '),(109,8,9,'Bewerten Sie die Technologie des Museums: 5D Zimmer'),(110,8,9,'Bewerten Sie die Technologie des Museums: Der Film “The eruption of the Vesuvius” '),(111,8,10,'Würden Sie Freunden und Verwandten einen Besuch empfehlen? '),(113,8,12,'Bewerten Sie Ihre Erfahrung in MAV'),(114,2,1,'Ti sei divertito durante la visita al Museo?'),(115,2,2,'Quale è la cosa che ti è piaciuta di più? '),(116,2,3,'E quella che ti è piaciuta di meno?'),(117,2,4,'Questo Museo lo ritieni adatto a te? '),(118,2,5,'Sei contento della guida che ti ha accompagnato durante il percorso? '),(119,2,6,'Ritorneresti volentieri al Museo? '),(120,2,7,'Consiglieresti il Museo ad un amico?'),(121,2,8,'Gli argomenti trattati durante il percorso sono stati chiari?'),(122,2,9,'Ti è piaciuta la Sala 5D?'),(123,3,1,'Come ha saputo dell\'esistenza del museo?'),(124,3,2,'È la prima volta che visita questo Museo?'),(125,3,3,'Indichi quali (una o più) delle seguenti definizioni rispecchia la sua esperienza di visita:'),(126,3,4,'Le tecnologie presenti nel museo sono utili alla sua comprensione delle antiche città di Pompei ed Ercolano?'),(127,3,5,'Come giudica l\'allestimento museale?'),(128,3,6,'Come giudica le seguenti sezioni del Museo: Cunicoli di scavo / Fogscree'),(133,3,6,'Come giudica le seguenti sezioni del Museo: Edifici pubblici / Teatro / Terme / Faro / Area sacra'),(134,3,6,'Come giudica le varie sezioni del museo: Domus /Casa del Poeta Tragico/ Casa del Citarista / Casa del Fauno / Casa del Labirinto '),(135,3,6,'Come giudica la seguente sezione del Museo: Villa dei papiri'),(136,3,7,'Come giudica il prezzo del ticket?'),(137,3,8,'Come giudica il seguente servizio: Front Office'),(138,3,8,'Come giudica i seguenti servizi?: Servizi / Toilettes'),(139,3,8,'Come giudica il seguente servizio: Shop'),(140,3,8,'Come giudica il seguente servizio: Bar'),(141,3,9,'Dopo questa esperienza ritornerebbe / Consiglierebbe una visita?'),(146,4,1,'È la prima volta che visita questo museo?'),(147,4,2,'Come è venuto a conoscenza del museo?'),(148,4,3,'La visita al museo ha soddisfatto le aspettative del gruppo?'),(149,4,4,'Consiglierebbe la visita a un suo conoscente/familiare?'),(150,4,5,'Indichi il livello di apprezzamento del seguente punto: Accoglienza da parte del personale'),(151,4,5,'Indichi il livello di apprezzamento del seguente punto: Prezzo'),(152,4,5,'Indichi il livello di apprezzamento del seguente punto: Durata della visita guidata'),(153,4,5,'Indichi il livello di apprezzamento del seguente punto: Qualità dei contenuti multimediali'),(154,4,5,'Indichi il livello di apprezzamento del seguente punto: Qualità dei supporti multimediali'),(155,4,5,'Indichi il livello di apprezzamento del seguente punto: Competenza/chiarezza delle guide'),(156,4,5,'Indichi il livello di apprezzamento del seguente punto: Servizio di prenotazione'),(157,4,5,'Indichi il livello di apprezzamento del seguente punto: Indicazioni per raggiungere il museo'),(158,4,5,'Indichi il livello di apprezzamento del seguente punto: Qualità degli ambienti'),(159,4,5,'Indichi il livello di apprezzamento del seguente punto: Accessibilità da parte di persone con'),(160,4,5,'Indichi il livello di apprezzamento del seguente punto: Qualità dei punti di ristoro'),(161,4,5,'Indichi il livello di apprezzamento del seguente punto: Qualità/pulizia dei servizi igienici'),(162,4,6,'Qual è il livello di gradimento in relazione all\'esperienza del nuovo allestimento'),(163,4,7,'Indichi il livello di apprezzamento della seguente area del museo: Cunicolo Borbonico '),(164,4,7,'Indichi il livello di apprezzamento della seguente area del museo: Edifici  pubblici'),(165,4,7,'Indichi il livello di apprezzamento della seguente area del museo: Domus'),(166,4,7,'Indichi il livello di apprezzamento della seguente area del museo: Villa Papiri'),(167,4,8,'Come giudica l\'esperienza nella sala 5D con il film sull\'Eruzione del Vesuvio?');
/*!40000 ALTER TABLE `question` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-04-11  8:58:14
