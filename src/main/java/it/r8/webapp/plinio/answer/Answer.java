package it.r8.webapp.plinio.answer;

import it.r8.webapp.plinio.core.BaseDomain;

public class Answer extends BaseDomain<Integer>{

	private static final long serialVersionUID = 1L;

	private Integer idQuestion;
	private String text;
	private Integer value;

	public Integer getIdQuestion() {
		return idQuestion;
	}
	
	public void setIdQuestion(Integer idQuestion) {
		this.idQuestion = idQuestion;
	}

	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}

	public Integer getValue() {
		return value;
	}
	public void setValue(Integer value) {
		this.value = value;
	}

	@Override
	public boolean equals(Object obj) {
		return getId().equals(((Answer)obj).getId());
	}
	
	

}
