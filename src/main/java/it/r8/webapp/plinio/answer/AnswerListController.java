package it.r8.webapp.plinio.answer;

import java.util.List;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;

import it.r8.webapp.plinio.core.Repository;

@ManagedBean
@ApplicationScoped
public class AnswerListController {
	
	private String test;
	
	@Inject
	private Repository <Integer, Answer> Repository;

	public String getTest() {
		return test;
	}

	public void setTest(String test) {
		this.test = test;
	}

	public List<Answer> getAll(int id) {
		return Repository.getAll();
	}

}
