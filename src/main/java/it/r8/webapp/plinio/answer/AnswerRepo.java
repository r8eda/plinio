package it.r8.webapp.plinio.answer;

import java.util.List;

import it.r8.webapp.plinio.core.Repository;

public interface AnswerRepo extends Repository <Integer, Answer>{
	
	List<Answer> getAllBySurvey(Integer idSurvey);

}