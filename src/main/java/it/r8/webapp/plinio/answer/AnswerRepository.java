package it.r8.webapp.plinio.answer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Singleton;
import javax.enterprise.context.Dependent;
import it.r8.webapp.plinio.core.DbConnection;

@Dependent
@Singleton
public class AnswerRepository extends DbConnection implements AnswerRepo {
	
	public void add(Answer answer) {
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)){
	    	Statement statement = connection.createStatement();
			String sql = String.format("INSERT INTO answer (idquestion,text, value) " + 
											  "VALUES ('%d','%s', '%d' )", answer.getId(),answer.getText(),
										      answer.getValue());
			statement.executeUpdate(sql);
	    } catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
	    }
	}
	


	
	public List<Answer> getAll() {
		List<Answer> answers = new ArrayList<Answer>();

	  	try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM answer");
	
	  		while (resultSet.next()) {
				Answer answer = new Answer();
				answer.setId(resultSet.getInt("id"));
				answer.setIdQuestion(resultSet.getInt("idquestion"));
				answer.setText(resultSet.getString("text"));
				answer.setValue(resultSet.getInt("value"));
				

				answers.add(answer);
			}
      	} catch (SQLException e) {
      		System.out.println("SQL exception");
      		e.printStackTrace();
      	}
	  	return answers;
	}
	
	public List<Answer> getAllBySurvey(Integer surveyId) {
		
		List<Answer> answers =  new ArrayList<Answer>();

	  	try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT a.*" + 
					"	FROM data d" + 
					"    INNER JOIN answer a ON d.idanswer = a.id" + 
					"    INNER JOIN question q ON a.idquestion = q.id" + 
					"    INNER JOIN survey s ON q.idsurvey = s.id" + 
					"    WHERE idSurvey =" + surveyId);
			
			while (resultSet.next()) {
				
				Answer answer = new Answer();
				answer.setId(resultSet.getInt("id"));
				answer.setIdQuestion(resultSet.getInt("idquestion"));
				answer.setText(resultSet.getString("text"));
				answer.setValue(resultSet.getInt("value"));
				answers.add(answer);
								
			}
			
	  	}catch (SQLException e) {
      		System.out.println("SQL exception");
      		e.printStackTrace();
      	}

	  	return answers;
	}
	
	public void update(Integer id, Answer answer) {
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			String sql = String.format("UPDATE answer a" +
									   "  SET idquestion = '%d'," +						   
									   "      text = '%s'  ," +
									   "	  value = '%d' " +
									   "WHERE a.id = %d", answer.getText(),
									   answer.getValue(),id);
			statement.executeUpdate(sql);
		
		} 
		catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}	
	}

	public void delete(Integer id) {
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			String sql = String.format("DELETE FROM answer WHERE id = %d", id);
				
			int result = statement.executeUpdate(sql);
			System.out.println(result);
				
		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}
	}
		
	public Answer getById(Integer id) {
		Answer answer = null;

		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			String sql = String.format("SELECT * FROM answer a WHERE a.id = '%s'", id);
				
			ResultSet resultSet = statement.executeQuery(sql);
	
			if (resultSet.first()) {
				answer = new Answer();
				answer.setId(resultSet.getInt("id"));
				answer.setIdQuestion(resultSet.getInt("idquestion"));
				answer.setText(resultSet.getString("text"));
				answer.setValue(resultSet.getInt("value"));
			}
			
		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}

		return answer;
	}

}