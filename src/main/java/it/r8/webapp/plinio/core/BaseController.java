package it.r8.webapp.plinio.core;

import java.io.IOException;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

public abstract class BaseController extends BaseDomain<Long>{


	private static final long serialVersionUID = 1L;

	protected Map<String, String>parameters;

	@PostConstruct 
	public void postConstruct() {
		parameters=FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		init();
	}

	public abstract void init();

	public void redirect(String pageUrl) {
		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect(pageUrl);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Map<String, String> getParameters() {
		return parameters;
	}

	public void setParameters(Map<String, String> parameters) {
		this.parameters = parameters;
	}	
}