package it.r8.webapp.plinio.core;

public abstract class DbConnection {
	protected static final String URL = "jdbc:mysql://localhost:3306/plinio?serverTimezone=UTC&autoReconnect=true&useSSL=false";
	protected static final String USER = "root";
	protected static final String PASSWORD = "test";
}
