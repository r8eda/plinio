package it.r8.webapp.plinio.core;

import java.io.Serializable;
import java.util.List;

import it.r8.webapp.plinio.core.BaseDomain;

public interface Repository<ID extends Serializable, D extends BaseDomain<ID>> {

	public void add(D entity);
	
	public List<D> getAll();
	
	public void update(ID id, D entity); 
	
	public void delete (ID id);
	
	public D getById(ID id);
}
