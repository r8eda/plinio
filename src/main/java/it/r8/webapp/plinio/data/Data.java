package it.r8.webapp.plinio.data;

import java.sql.Timestamp;

import it.r8.webapp.plinio.core.BaseDomain;

public class Data extends BaseDomain<Integer>{
	
	private static final long serialVersionUID = 1L;
	
	private Integer idAnswer;
	private Integer idQuestion;
	private Timestamp date;
	
	public Integer getIdAnswer() {
		return idAnswer;
	}

	public void setIdAnswer(Integer idAnswer) {
		this.idAnswer = idAnswer;
	}

	public Integer getIdQuestion() {
		return idQuestion;
	}

	public void setIdQuestion(Integer idQuestion) {
		this.idQuestion = idQuestion;
	}

	public Timestamp getDate() {
		return date;
	}
	
	public void setDate(Timestamp date) {
		this.date = date;
	}
}
