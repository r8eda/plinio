package it.r8.webapp.plinio.data;

import it.r8.webapp.plinio.core.Repository;

public interface DataRepo extends Repository<Integer, Data> {

}
