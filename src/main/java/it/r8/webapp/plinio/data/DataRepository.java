package it.r8.webapp.plinio.data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Singleton;
import javax.enterprise.context.Dependent;

import it.r8.webapp.plinio.answer.Answer;
import it.r8.webapp.plinio.core.DbConnection;
import it.r8.webapp.plinio.core.Repository;
import it.r8.webapp.plinio.question.Question;

@Dependent
@Singleton
public class DataRepository extends DbConnection implements Repository <Integer,Data>{
	
	public void add(Data data) {
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)){
	    	Statement statement = connection.createStatement();
			String sql = String.format("INSERT INTO data(idquestion, idanswer, date) " + 
											  "VALUES ('%d','%d', current_date())", data.getIdQuestion(),
										      data.getIdAnswer());
			statement.executeUpdate(sql);
	    } catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
	    }
	}
	
	@Override
	public List<Data> getAll() {
		List<Data> questions = new ArrayList<Data>();

	  	try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT d.id AS idData, d.idquestion AS idQuestion, a.id AS idAnswer, a.text AS textAnswer" +
														 "  FROM data d INNER JOIN answer a ON d.idanswer = a.id");

	  		while (resultSet.next()) {
				Data data = new Data();
	  			Question question = new Question();
	  			Answer answer = new Answer();
	  			
				data.setId(resultSet.getInt("idData"));
				question.setId(resultSet.getInt("idquestion"));
				
				answer.setId(resultSet.getInt("idAnswer"));
				answer.setText(resultSet.getString("textAnswer"));
				
				
				// questions.add(question);
			}
      	} catch (SQLException e) {
      		System.out.println("SQL exception");
      		e.printStackTrace();
      	}
	  	return questions;
	}
	
	public List<Data> getAllByQuestion(Integer questionId) {
		Map<Answer, Integer> questions = new LinkedHashMap<>();
		
	  	try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT d.id AS idData, d.idquestion AS idQuestion, a.id AS idAnswer, a.text AS textAnswer" +
														 "  FROM data d INNER JOIN answer a ON d.idanswer = a.id and d.idquestion = 1");

	  		while (resultSet.next()) {
	  			Answer answer = new Answer();
				answer.setId(resultSet.getInt("idAnswer"));
				answer.setText(resultSet.getString("textAnswer"));

				// questions.add(question);
			}
      	} catch (SQLException e) {
      		System.out.println("SQL exception");
      		e.printStackTrace();
      	}
		
		return null;
	}

	@Override
	public void update(Integer id, Data entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Data getById(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}
}
