package it.r8.webapp.plinio.email;

import it.r8.webapp.plinio.core.BaseDomain;

public class Visitor extends BaseDomain<Integer>{
	
	private static final long serialVersionUID = 1L;
	
	private String email;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
