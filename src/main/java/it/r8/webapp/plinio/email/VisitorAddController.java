package it.r8.webapp.plinio.email;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import it.r8.webapp.plinio.core.Repository;

@ManagedBean
@ViewScoped
public class VisitorAddController {
	
	private Visitor visitor;
	
	@Inject
	private Repository<Integer,Visitor> visitorRepository;
	
	@PostConstruct
	public void init() {
		visitor = new Visitor();
	}
	
	public void addVisitor() {
		visitorRepository.add(visitor);
	}

	public Visitor getVisitor() {
		return visitor;
	}

	public void setVisitor(Visitor visitor) {
		this.visitor = visitor;
	}

}
