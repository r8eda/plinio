package it.r8.webapp.plinio.email;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Singleton;
import javax.enterprise.context.Dependent;

import it.r8.webapp.plinio.core.DbConnection;
import it.r8.webapp.plinio.core.Repository;

@Dependent
@Singleton
public class VisitorRepository extends DbConnection implements Repository<Integer, Visitor>{

	public void add (Visitor visitor) {
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)){
	    	Statement statement = connection.createStatement();
			String sql = String.format("INSERT INTO visitor (email) " + 
											  "VALUES ('%s')", visitor.getEmail());
			statement.executeUpdate(sql);
	    } catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
	    }
	}
	
	public List<Visitor> getAll() {
		List<Visitor> visitors = new ArrayList<Visitor>();

	  	try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM visitor");
	
	  		while (resultSet.next()) {
				Visitor visitor = new Visitor();
				visitor.setId(resultSet.getInt("id"));
				visitor.setEmail(resultSet.getString("email"));
				
				visitors.add(visitor);
			}
      	} catch (SQLException e) {
      		System.out.println("SQL exception");
      		e.printStackTrace();
      	}
	  	return visitors;
	}

	@Override
	public void update(Integer id, Visitor entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Visitor getById(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	
}
