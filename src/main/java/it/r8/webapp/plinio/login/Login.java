package it.r8.webapp.plinio.login;

import javax.validation.constraints.Size;

import it.r8.webapp.plinio.core.BaseDomain;

public class Login extends BaseDomain<Integer>{
	
	private static final long serialVersionUID = 1L;
	
	@Size(min = 6, max = 15, message = "La lunghezza deve essere compresa tra 6 e 15")
	private String username;
	@Size(min = 6, max = 15, message = "La lunghezza deve essere compresa tra 6 e 15")
	private String password;
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
}
