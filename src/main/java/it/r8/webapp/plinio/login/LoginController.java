package it.r8.webapp.plinio.login;

import java.io.IOException;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ViewScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import it.r8.webapp.plinio.core.Repository;

@ManagedBean
@ViewScoped
public class LoginController {
	
	@Inject
	private Repository <Integer, Login> Repository;
	String username;
	String password;
	
	public void login(){
		FacesContext context = FacesContext.getCurrentInstance();
		boolean access=false;
		List<Login> users = Repository.getAll();
		
		for(Login userReaded : users) {
			if ( this.password.equals(userReaded.getPassword()) && this.username.equals(userReaded.getUsername())) {
				context.getExternalContext().getSessionMap().put("user", username);
				try {
					context.getExternalContext().redirect("adminhome.xhtml");
				} catch (IOException e) {
					e.printStackTrace();
				}
				access=true;
			}	
		}
		
		if (!access){
			context.addMessage(null, new FacesMessage("Autenticazione Fallita ! Username o Password errati"));
		}
	}
	
	public void logout() {
    	FacesContext context = FacesContext.getCurrentInstance();
    	context.getExternalContext().invalidateSession();
        try {
			context.getExternalContext().redirect("login.xhtml");
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
