package it.r8.webapp.plinio.login;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Singleton;
import javax.enterprise.context.Dependent;

import it.r8.webapp.plinio.core.DbConnection;
import it.r8.webapp.plinio.core.Repository;

@Dependent
@Singleton
public class LoginRepository extends DbConnection implements Repository<Integer, Login>{

	@Override
	public void add(Login entity) {
		// TODO Auto-generated method stub
		
	}

	public List<Login> getAll() {
		List<Login> users = new ArrayList<Login>();

	  	try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM login");
	
	  		while (resultSet.next()) {
				Login login = new Login();
				login.setId(resultSet.getInt("id"));
				login.setUsername(resultSet.getString("username"));
				login.setPassword(resultSet.getString("password"));
				
				users.add(login);
			}
      	} catch (SQLException e) {
      		System.out.println("SQL exception");
      		e.printStackTrace();
      	}
	  	return users;
	}

	@Override
	public void update(Integer id, Login entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Login getById(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}
	
	

}
