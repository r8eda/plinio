package it.r8.webapp.plinio.question;

import java.util.List;

import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import it.r8.webapp.plinio.answer.Answer;
import it.r8.webapp.plinio.core.BaseDomain;

public class Question extends BaseDomain<Integer>{

	private static final long serialVersionUID = 1L;
	
	private Integer idSurvey;
	
	@Positive
	private Integer number;
	
	@Size(min = 10, max = 250, message = "La lunghezza deve essere compresa tra 10 e 250")
	private String text;
	
	private List<Answer> answers;

	public Integer getIdSurvey() {
		return idSurvey;
	}

	public void setIdSurvey(Integer idSurvey) {
		this.idSurvey = idSurvey;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public List<Answer> getAnswers() {
		return answers;
	}

	public void setAnswers(List<Answer> answers) {
		this.answers = answers;
	}
}
   