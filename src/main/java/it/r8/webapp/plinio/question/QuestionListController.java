package it.r8.webapp.plinio.question;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import it.r8.webapp.plinio.core.BaseController;
import it.r8.webapp.plinio.core.Repository;
import it.r8.webapp.plinio.data.Data;

@ManagedBean
@ViewScoped
public class QuestionListController extends BaseController {
	
private static final long serialVersionUID = 1L;
	
	@Inject
	private QuestionRepo questionRepository;
		
	@Inject
	private Repository<Integer, Data> dataRepository;
		
	private List<Question> questions;
	
	private Map<Integer, Integer> choices = new HashMap<>();
	
	@Override
	public void init() {
		Integer idSurvey = Integer.valueOf(parameters.get("idSurvey"));
		questions = questionRepository.getAllBySurvey(idSurvey);
	}
	
	public void addSurvey() {
		for(Entry<Integer, Integer> selectedChoice : choices.entrySet()) {			
			Data data = new Data();
			data.setIdQuestion(selectedChoice.getKey());
			data.setIdAnswer(Integer.valueOf(String.valueOf(selectedChoice.getValue())));
			
			dataRepository.add(data);

		}	
	}

	public boolean enableToSend() {
		return !(choices.size() == questions.size());
	}
	
	public List<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}

	public Map<Integer, Integer> getChoices() {
		return choices;
	}

	public void setChoices(Map<Integer, Integer> choices) {
		this.choices = choices;
	}
	
	public String getId(Question question) {
		return "" + System.currentTimeMillis();
	}

}
