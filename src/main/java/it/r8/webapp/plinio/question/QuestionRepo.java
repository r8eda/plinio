package it.r8.webapp.plinio.question;

import java.util.List;

import it.r8.webapp.plinio.core.Repository;

public interface QuestionRepo extends Repository <Integer, Question> {
	
	List<Question> getAllBySurvey(Integer idSurvey);

}
