package it.r8.webapp.plinio.question;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Singleton;
import javax.enterprise.context.Dependent;

import it.r8.webapp.plinio.answer.Answer;
import it.r8.webapp.plinio.core.DbConnection;

@Dependent
@Singleton
public class QuestionRepository extends DbConnection implements QuestionRepo {
		
	public void add(Question question) {
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)){
	    	Statement statement = connection.createStatement();
			String sql = String.format("INSERT INTO question(number,text) " + 
											  "VALUES ('%d','%s')", question.getNumber(),
										      question.getText());
			statement.executeUpdate(sql);
	    } catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
	    }
	}

	public List<Question> getAll() {
		List<Question> questions = new ArrayList<Question>();
	  	try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM question");
		
	  		while (resultSet.next()) {
				Question question = new Question();
				question.setId(resultSet.getInt("question.id"));
				question.setText(resultSet.getString("text"));
				question.setNumber(resultSet.getInt("number"));
				questions.add(question);
			}
      	} catch (SQLException e) {
      		System.out.println("SQL exception");
      		e.printStackTrace();
      	}
	  	return questions;
	}
		
	public List<Question> getAllBySurvey(Integer surveyId) {
			
		Map<Integer, Question> questions = new LinkedHashMap<>();

	  	try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT q.id AS idQuestion, " + 
					"		a.id AS idAnswer," + 
					"        q.text AS textQuestion," + 
					"        a.text AS textAnswer," + 
					"        a.value" + 
					" FROM answer a" + 
					" INNER JOIN question q ON a.idquestion = q.id" + 
					" INNER JOIN survey s on q.idsurvey = s.id" + 
					" WHERE s.id = " + surveyId +
					" order by q.number,a.value");
				
		
	  		while (resultSet.next()) {
	  			Answer answer = new Answer();

				answer.setId((Integer)resultSet.getInt("idAnswer"));
				answer.setText((String)resultSet.getString("textAnswer"));

				if (!questions.containsKey((Integer)resultSet.getInt("idQuestion"))) {
					Question question = new Question();

					List<Answer> answers = new ArrayList<Answer>();
					answers.add(answer);

					question.setAnswers(answers);
					question.setId((Integer)resultSet.getInt("idQuestion"));
					question.setText((String)resultSet.getString("textQuestion"));

					questions.put((Integer)resultSet.getInt("idQuestion"), question);

				} else {
					Question question = questions.get((Integer)resultSet.getInt("idQuestion"));
					question.getAnswers().add(answer);
				}
			}
		  		
      	} catch (SQLException e) {
      		System.out.println("SQL exception");
      		e.printStackTrace();
      	}
		  	
	  	return new ArrayList<Question>(questions.values());
	}
		
	public void update(Integer id, Question question) {
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			String sql = String.format("UPDATE question q" +
									   "  SET number = '%d'  ," +
									   "	  text = '%s' " +
									   "WHERE q.id = %d", question.getNumber(),
									   question.getText(),id);
			statement.executeUpdate(sql);
			
		} 
		catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}	
	}

	public void delete(Integer id) {
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			String sql = String.format("DELETE FROM question WHERE id = %d", id);
					
			int result = statement.executeUpdate(sql);
			System.out.println(result);
					
		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}
	}
			
	public Question getById(Integer id) {
		Question question= null;

		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			String sql = String.format("SELECT * FROM question q WHERE q.id = '%s'", id);
					
			ResultSet resultSet = statement.executeQuery(sql);
		
			if (resultSet.first()) {
				question = new Question();
				question.setNumber(resultSet.getInt("number"));
				question.setText(resultSet.getString("text"));
					
			}
				
		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}

		return question;
	}

}