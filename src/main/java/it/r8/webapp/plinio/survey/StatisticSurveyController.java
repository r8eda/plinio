package it.r8.webapp.plinio.survey;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import it.r8.webapp.plinio.core.BaseController;

@ManagedBean
@ViewScoped
public class StatisticSurveyController  extends BaseController {
	
	private static final long serialVersionUID = 1L;
	
	private List<StatisticSurveyData> statisticsSurveyData;
	
	@Inject
	private SurveyRepo surveyRepository;
	
	@Override
	public void init() {
		Integer idSurvey = Integer.valueOf(parameters.get("idSurvey"));
		statisticsSurveyData = surveyRepository.getAllBySurvey(idSurvey);
	}
	
	public List<StatisticSurveyData> getStatisticsSurveyData() {
		return statisticsSurveyData;
	}

	public void setStatisticsSurveyDeta(List<StatisticSurveyData> statisticsSurveyData) {
		this.statisticsSurveyData = statisticsSurveyData;
	}

	public String getPieChartData(StatisticSurveyData statisticSurveyData) {
		StringBuilder stringBuilder = new StringBuilder();

		for (AnswerSelected answerSelected : statisticSurveyData.getAnswersSelected()) {
			stringBuilder.append("[");
			stringBuilder.append("'");
			stringBuilder.append(answerSelected.getText());
			stringBuilder.append("'");
			stringBuilder.append(",");
			stringBuilder.append(answerSelected.getOccurrence());
			stringBuilder.append("]");
			stringBuilder.append(",");
		}
		
		return stringBuilder.toString();
	}
}
