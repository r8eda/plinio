package it.r8.webapp.plinio.survey;

import java.sql.Timestamp;
import java.util.List;

import it.r8.webapp.plinio.core.BaseDomain;
import it.r8.webapp.plinio.question.Question;

public class StatisticSurveyData extends BaseDomain<Integer> {
	
	private static final long serialVersionUID = 1L;
		
	private Question question;
	
	private List<AnswerSelected> answersSelected;
	
	private Timestamp date;
	
	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	public List<AnswerSelected> getAnswersSelected() {
		return answersSelected;
	}

	public void setAnswersSelected(List<AnswerSelected> answersSelected) {
		this.answersSelected = answersSelected;
	}

	public Timestamp getDate() {
		return date;
	}

	public void setDate(Timestamp date) {
		this.date = date;
	}
}
