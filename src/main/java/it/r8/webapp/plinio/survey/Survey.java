package it.r8.webapp.plinio.survey;

import javax.validation.constraints.Size;
import it.r8.webapp.plinio.core.BaseDomain;

public class Survey extends BaseDomain<Integer> {
	
	private static final long serialVersionUID = 1L;
	
	@Size(min = 3, max = 20, message = "La lunghezza deve essere compresa tra 3 e 20")
	private String typology;
	
	public String getTypology() {
		return typology;
	}

	public void setTypology(String typology) {
		this.typology = typology;
	}
}
