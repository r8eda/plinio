package it.r8.webapp.plinio.survey;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

@ManagedBean
@ViewScoped
public class SurveyListController {

private String test;
	
	@Inject
	private SurveyRepo surveyRepository;

	public String getTest() {
		return test;
	}

	public void setTest(String test) {
		this.test = test;
	}

	public List<Survey> getSurveys() {
		return surveyRepository.getAll();
	}

}