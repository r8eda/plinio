package it.r8.webapp.plinio.survey;

import java.util.List;

import it.r8.webapp.plinio.core.Repository;

public interface SurveyRepo extends Repository <Integer,Survey>{
	
	List<StatisticSurveyData> getAllBySurvey(Integer surveyId);
	
	StatisticSurveyData getAllByQuestion(Integer questionId);
}
