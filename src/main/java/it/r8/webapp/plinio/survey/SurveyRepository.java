package it.r8.webapp.plinio.survey;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ejb.Singleton;
import javax.enterprise.context.Dependent;

import it.r8.webapp.plinio.answer.Answer;
import it.r8.webapp.plinio.core.DbConnection;
import it.r8.webapp.plinio.question.Question;

@Dependent
@Singleton
public class SurveyRepository extends DbConnection implements SurveyRepo {
	
	public List<Survey> getAll() {
		List<Survey> surveys = new ArrayList<Survey>();

	  	try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM survey");
	
	  		while (resultSet.next()) {
				Survey survey = new Survey();
				survey.setId(resultSet.getInt("id"));
				survey.setTypology(resultSet.getString("typology"));
				surveys.add(survey);
			}
      	} catch (SQLException e) {
      		System.out.println("SQL exception");
      		e.printStackTrace();
      	}
	  	return surveys;
	}

	public List<StatisticSurveyData> getAllBySurvey(Integer surveyId) {

		Map<Question, Map<Answer, Integer>> questions = new LinkedHashMap<>();


		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT d.id AS idData, d.idquestion AS idQuestion," +
					" q.text AS textQuestion, a.id AS idAnswer, a.text AS textAnswer" +
					" FROM data d" +
					" INNER JOIN answer a ON d.idanswer = a.id" +
					" INNER JOIN question q ON d.idquestion = q.id" +
					" INNER JOIN survey s ON q.idsurvey = s.id and s.id = " + surveyId);

			while (resultSet.next()) {
				Answer answer = new Answer();
				answer.setId(resultSet.getInt("idAnswer"));
				answer.setText(resultSet.getString("textAnswer"));

				Question question = new Question();
				question.setId(resultSet.getInt("idQuestion"));
				question.setText(resultSet.getString("textQuestion"));

				if (questions.containsKey(question)) {
					Map<Answer, Integer> answers = questions.get(question);

					if (answers.containsKey(answer)) {
						answers.put(answer, answers.get(answer) + 1);

					} else {
						answers.put(answer, 1);
					}

				} else {
					HashMap<Answer, Integer> answers = new LinkedHashMap<>();
					answers.put(answer, 1);
					
					questions.put(question, answers);
				}
			}

		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}


		List<StatisticSurveyData> statisticsSurveyData = new ArrayList<>();

		for(Entry<Question, Map<Answer, Integer>> question : questions.entrySet()) {
			StatisticSurveyData statisticSurveyData = new StatisticSurveyData();
			statisticSurveyData.setId(question.getKey().getId());
			statisticSurveyData.setQuestion(question.getKey());

			List<AnswerSelected> answersSelected = new ArrayList<>();

			for (Entry<Answer, Integer> answer : question.getValue().entrySet()) {
				AnswerSelected answerSelected = new AnswerSelected();
				answerSelected.setId(answer.getKey().getId());
				answerSelected.setText(answer.getKey().getText());
				answerSelected.setOccurrence(answer.getValue());

				answersSelected.add(answerSelected);
			}

			statisticSurveyData.setAnswersSelected(answersSelected);
			statisticsSurveyData.add(statisticSurveyData);
		}

		return statisticsSurveyData;
	}

	@Override
	public StatisticSurveyData getAllByQuestion(Integer questionId) {
		Map<Answer, Integer> answers = new LinkedHashMap<>();

		StatisticSurveyData data = new StatisticSurveyData();

		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT d.id AS idData, d.idquestion AS idQuestion, " +
					" q.text AS textQuestion, a.id AS idAnswer, a.text AS textAnswer " +
					" FROM data d " +
					" INNER JOIN answer a ON d.idanswer = a.id" +
					" INNER JOIN question q ON d.idquestion = q.id and d.idquestion = 1");

			while (resultSet.next()) {
				Answer answer = new Answer();
				answer.setId(resultSet.getInt("idAnswer"));
				answer.setText(resultSet.getString("textAnswer"));

				Question question = new Question();
				question.setId(resultSet.getInt("idQuestion"));
				question.setText(resultSet.getString("textQuestion"));

				data.setQuestion(question);

				if (answers.containsKey(answer)) {
					answers.put(answer, answers.get(answer) + 1);

				} else {
					answers.put(answer, 1);
				}

			}
		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}

		List<AnswerSelected> answersSelected = new ArrayList<>();

		for(Entry<Answer, Integer> entry : answers.entrySet()) {
			AnswerSelected answerSelected = new AnswerSelected();
			answerSelected.setId(entry.getKey().getId());
			answerSelected.setText(entry.getKey().getText());
			answerSelected.setOccurrence(entry.getValue());

			answersSelected.add(answerSelected);
		}

		data.setAnswersSelected(answersSelected); 

		return data;
	}

	@Override
	public void add(Survey entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(Integer id, Survey entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Survey getById(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}
}
